resource "google_compute_router_nat" "nat" {
  project                            = var.project
  min_ports_per_vm                   = var.nat_ports_per_vm
  name                               = var.nat_name == "" ? "nat-${random_id.id.hex}" : var.nat_name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = concat(google_compute_address.nat_ips[*].self_link, data.google_compute_address.imported_ips[*].self_link, data.google_compute_address.secondary_imported_ips[*].self_link)
  region                             = var.region
  router                             = google_compute_router.router.name
  source_subnetwork_ip_ranges_to_nat = length(var.subnetworks) > 0 ? "LIST_OF_SUBNETWORKS" : "ALL_SUBNETWORKS_ALL_IP_RANGES"

  dynamic "subnetwork" {
    for_each = var.subnetworks
    content {
      name                    = data.google_compute_subnetwork.subnetworks[subnetwork.key].self_link
      source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
    }
  }

  log_config {
    filter = var.log_level != "NONE" ? var.log_level : "ERRORS_ONLY"
    enable = var.log_level != "NONE"
  }
}

resource "google_compute_address" "nat_ips" {
  project = var.project
  count   = var.nat_ip_count

  name = format(
    "%s-%02d",
    var.ip_name_prefix == "" ? (var.nat_name == "" ? "nat-${random_id.id.hex}" : var.nat_name) : var.ip_name_prefix,
    count.index + 1,
  )
  region = var.region
}

resource "google_compute_router" "router" {
  project = var.project
  name    = var.router_name == "" ? "nat-router-${random_id.id.hex}" : var.router_name
  network = var.network_self_link
  region  = var.region
}

data "google_compute_subnetwork" "subnetworks" {
  project = var.project
  count   = length(var.subnetworks)

  name   = var.subnetworks[count.index]
  region = var.region
}

resource "random_id" "id" {
  byte_length = 2
}

data "google_compute_address" "imported_ips" {
  project = var.project
  count   = var.imported_ip_count

  name   = format("%s-%s", var.imported_ip_prefix, replace(cidrhost(var.imported_ip_cidr, var.imported_ip_start_index + count.index), ".", "-"))
  region = var.region
}

data "google_compute_address" "secondary_imported_ips" {
  project = var.project
  count   = var.secondary_imported_ip_count

  name   = format("%s-%s", var.secondary_imported_ip_prefix, replace(cidrhost(var.secondary_imported_ip_cidr, var.secondary_imported_ip_start_index + count.index), ".", "-"))
  region = var.region
}
