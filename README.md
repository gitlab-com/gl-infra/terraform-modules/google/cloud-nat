# GitLab.com Cloud NAT Terraform Module

## What is this?

This module provisions a Google Cloud NAT router.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.59 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.59 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 3.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_address.nat_ips](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_router.router](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router) | resource |
| [google_compute_router_nat.nat](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router_nat) | resource |
| [random_id.id](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [google_compute_address.imported_ips](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_address) | data source |
| [google_compute_address.secondary_imported_ips](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_address) | data source |
| [google_compute_subnetwork.subnetworks](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_subnetwork) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_imported_ip_cidr"></a> [imported\_ip\_cidr](#input\_imported\_ip\_cidr) | n/a | `string` | `""` | no |
| <a name="input_imported_ip_count"></a> [imported\_ip\_count](#input\_imported\_ip\_count) | n/a | `number` | `0` | no |
| <a name="input_imported_ip_prefix"></a> [imported\_ip\_prefix](#input\_imported\_ip\_prefix) | n/a | `string` | `""` | no |
| <a name="input_imported_ip_start_index"></a> [imported\_ip\_start\_index](#input\_imported\_ip\_start\_index) | n/a | `number` | `0` | no |
| <a name="input_ip_name_prefix"></a> [ip\_name\_prefix](#input\_ip\_name\_prefix) | Prefix to use for the names of provisioned static IPs. | `string` | `""` | no |
| <a name="input_log_level"></a> [log\_level](#input\_log\_level) | See https://www.terraform.io/docs/providers/google/r/compute_router_nat.html#filter for valid values, or pass "NONE" to disable all logging. | `string` | `"ERRORS_ONLY"` | no |
| <a name="input_nat_ip_count"></a> [nat\_ip\_count](#input\_nat\_ip\_count) | This variable should be set according to the following formula:  nat\_ip\_count = M * P / 64,512  Where:  M = number of machines in the region (multiply by some generous number to account for future growth) P = NAT ports per VM (see a variable below)  https://cloud.google.com/nat/docs/overview#number_of_nat_ports_and_connections | `number` | `0` | no |
| <a name="input_nat_name"></a> [nat\_name](#input\_nat\_name) | n/a | `string` | `""` | no |
| <a name="input_nat_ports_per_vm"></a> [nat\_ports\_per\_vm](#input\_nat\_ports\_per\_vm) | TCP and UDP NAT ports (both have this count) | `number` | `0` | no |
| <a name="input_network_self_link"></a> [network\_self\_link](#input\_network\_self\_link) | n/a | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | `null` | no |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | n/a | yes |
| <a name="input_router_name"></a> [router\_name](#input\_router\_name) | n/a | `string` | `""` | no |
| <a name="input_secondary_imported_ip_cidr"></a> [secondary\_imported\_ip\_cidr](#input\_secondary\_imported\_ip\_cidr) | n/a | `string` | `""` | no |
| <a name="input_secondary_imported_ip_count"></a> [secondary\_imported\_ip\_count](#input\_secondary\_imported\_ip\_count) | n/a | `number` | `0` | no |
| <a name="input_secondary_imported_ip_prefix"></a> [secondary\_imported\_ip\_prefix](#input\_secondary\_imported\_ip\_prefix) | n/a | `string` | `""` | no |
| <a name="input_secondary_imported_ip_start_index"></a> [secondary\_imported\_ip\_start\_index](#input\_secondary\_imported\_ip\_start\_index) | n/a | `number` | `0` | no |
| <a name="input_subnetworks"></a> [subnetworks](#input\_subnetworks) | Set this to cover a particular list of subnetworks as opposed to a whole region. | `list(string)` | `[]` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
