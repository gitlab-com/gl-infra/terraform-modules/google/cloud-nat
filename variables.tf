variable "project" {
  type    = string
  default = null
}

variable "network_self_link" {
  type = string
}

variable "region" {
  type = string
}

variable "subnetworks" {
  type        = list(string)
  default     = []
  description = "Set this to cover a particular list of subnetworks as opposed to a whole region."
}

# This variable should be set according to the following formula:
#
# nat_ip_count = M * P / 64,512
#
# Where:
#
# M = number of machines in the region (multiply by some generous number to account for future growth)
# P = NAT ports per VM (see a variable below)
#
# https://cloud.google.com/nat/docs/overview#number_of_nat_ports_and_connections
variable "nat_ip_count" {
  type    = number
  default = 0
}

# The following 3 variables, starting with imported_ip, allow us to assign
# existing static IPs to the Cloud NAT. This would be useful if we had a
# contiguous range of static IPs in our project that we wanted to use for NAT
# instances.
#
# The IPs must be named PREFIX-A-B-C-D where A, B, C, and D are decimal octets
# corresponding to an IPv4 address.
#
# For example, if we have static IPs named IP-100-0-0-1, IP-100-0-0-2 that are
# part of the 100.0.0.0/24 CIDR range, and we wanted to use IPs 100.0.0.10-19
# for one NAT instance, we'd set:
#
# imported_ip_prefix = "IP"
# imported_ip_cidr = "100.0.0.0/24"
# imported_ip_start_index = 10
# imported_ip_count = 10
#
# nat_ip_count could then be set to 0 to avoid provisioning extra IPs.

variable "imported_ip_prefix" {
  type    = string
  default = ""
}

variable "imported_ip_cidr" {
  type    = string
  default = ""
}

variable "imported_ip_start_index" {
  type    = number
  default = 0
}

variable "imported_ip_count" {
  type    = number
  default = 0
}

variable "secondary_imported_ip_prefix" {
  type    = string
  default = ""
}

variable "secondary_imported_ip_cidr" {
  type    = string
  default = ""
}

variable "secondary_imported_ip_start_index" {
  type    = number
  default = 0
}

variable "secondary_imported_ip_count" {
  type    = number
  default = 0
}

# The GCP default is 64. This module default of 0 will cause the resource to
# take the GCP default, but permits importing existing resources without a plan
# diff "min_ports_per_vm = 0 -> 64", which is marked as forcing replacement
# (even though this can be patched at the GCP API level).
#
# https://cloud.google.com/nat/docs/overview#number_of_nat_ports_and_connections
variable "nat_ports_per_vm" {
  type        = number
  description = "TCP and UDP NAT ports (both have this count)"
  default     = 0
}

# See
# https://www.terraform.io/docs/providers/google/r/compute_router_nat.html#filter
# for valid values, or pass "NONE" to disable all logging.
variable "log_level" {
  type    = string
  default = "ERRORS_ONLY"
}

# The name variables exist in order to import NAT resources from existing
# infrastructure.

variable "nat_name" {
  type    = string
  default = ""
}

variable "router_name" {
  type    = string
  default = ""
}

# This doesn't normally need to be set, and defaults to the NAT name, which
# itself can be overridden.
variable "ip_name_prefix" {
  type        = string
  default     = ""
  description = "Prefix to use for the names of provisioned static IPs."
}
